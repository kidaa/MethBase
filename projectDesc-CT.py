# projectDesc: Generate an html file with summary of samples from a
#              given project
#
# Copyright (C) 2013-2017 University of Southern California and
#                         Song Qiang <keeyang@ustc.edu>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#! /usr/bin/env python

import sys
import os
import glob
import re
import yaml

from MethBase import Sample
from NCBI import PubMedArticle
import MethBase
import MethBase_Utils
import argparse

def getSampleDirs(file_organization, celltype, wkdir, assembly):
    rootdir = '/labdata/methylome/public'
    samplelinks = []
    projects = []
    IN = open(file_organization, 'r')
    struct_dirs = yaml.load(IN)

    for p in struct_dirs[assembly][celltype].keys():
        samplelinks += [os.path.join(rootdir, p, s, 'results_'+assembly) \
                        for s in struct_dirs[assembly][celltype][p] ]
        projdir = os.path.join(wkdir, p)
        projects.append(MethBase.Project(projdir))
    IN.close()
    return samplelinks, projects

def get_html_header(s):
    desc = """
<html>
<head>
<title>{ID}</title>
</head>
<body>

"""
    return desc.format(ID = s)
 
def get_conventions():
    return """
<div style="width:800px;">
<br><b>Terms of use</b>: If you use this resource, please cite us! The Smith Lab at USC has developed and is owner of all analyses and associated browser tracks from the MethBase database (e.g. tracks displayed in the "DNA Methylation" trackhub on the UCSC Genome Browser). Any derivative work or use of the MethBase resource that appears in published literature must cite the most recent publication associated with Methbase (see "References" below). Users who wish to copy the contents of MethBase in bulk into a publicly available resource must additionally have explicit permission from the Smith Lab to do so. We hope the MethBase resource can help you!
</div>

<h2>Display Conventions and Configuration</h2> 
<div style="width:800px;">
<p>

The various types of tracks associated with a methylome follow the
display conventions below.

Green intervals represent partially methylated region; Blue intervals
represent hypo-methylated regions; Yellow bars represent methylation
levels; Black bars represent depth of coverage; Purple intervals
represent allele-specific methylated regions; Purple bars represent
allele-specific methylation score; and red intervals represent
hyper-methylated regions.

</p>
</div>
"""

                       
def get_methods():
    return """
<h2>Methods</h2> 
<div style="width:800px;">
<p>
All analysis was done using a bisulfite sequnecing data analysis
pipeline <a href="http://smithlabresearch.org/software/methpipe/">MethPipe</a> developed in the <a href="http://smithlabresearch.org/">Smith lab</a> at USC.
</p>
<p>
<b>Mapping bisulfite treated reads:</b> Bisulfite treated reads are mapped
to the genomes with the <code>rmapbs</code> program (<code>rmapbs-pe</code> for pair-end
reads), one of the wildcard based mappers. Input reads are filtered by
their quality, and adapter sequences in the 3' end of reads are
trimmed. Uniquely mapped reads with mismatches below given threshold
are kept. For pair-end reads, if the two mates overlap, the
overlapping part of the mate with lower quality is clipped. After
mapping, we use the program <code>duplicate-remover</code> to randomly select one
from multiple reads mapped exactly to the same location.
</p>
<p>
<b>Estimating methylation levels:</b> After reads are mapped and filtered,
the <code>methcounts</code> program is used to obtain read coverage and estimate
methylation levels at individual cytosine sites. We count the number
of methylated reads (containing C's) and the number of unmethylated
reads (containing T's) at each cytosine site.  The methylation level
of that cytosine is estimated with the ratio of methylated to total
reads covering that cytosine. For cytosines within the symmetric CpG
sequence context, reads from the both strands are used to give a
single estimate.
</p>
<p>
<b>Estimating bisulfite conversion rate:</b> Bisulfite conversion rate is
 estimated with the <code>bsrate</code> program by computing the fraction of
 successfully converted reads (read out as Ts) among all reads mapped
 to presumably unmethylated cytosine sites, for example, spike-in
 lambda DNA, chroloplast DNA or non-CpG cytosines in mammalian
 genomes.
</p>
<p>
<b>Identifying hypo-methylated regions:</b> In most mammalian cells, the
majority of the genome has high methylation, and regions of low
methylation are typically more interesting. These are called
hypo-methylated regions (HMR).  To identify the HMRs, we use the <code>hmr</code>
which implements a hidden Markov model (HMM) approach taking into
account both coverage and methylation level information.
</p>
<p>
<b>Identifying hyper-methylated regions:</b> Hyper-methylated regions
 (HyperMR) are of interest in plant methylomes, invertebrate
 methylomes and other methylomes showing "mosaic methylation"
 pattern. We identify HyperMRs with the <code>hmr_plant</code> program for those
 samples showing "mosaic methylation" pattern. 
</p>
<p>
<b>Identifying partially methylated domains:</b> Partially methylated
 domains are large genomic regions showing partial methylation
 observed in immortalized cell lines and cancerous cells. The <code>pmd</code>
 program is used to identify PMDs. 
</p>
<p>
<b>Identifying allele-specific methylated regions:</b> Allele-Specific
 methylated regions refers to regions where the parental allele is
 differentially methylated compared to the maternal allele. The
 program <code>allelicmeth</code> is used to allele-specific methylation score
 can be computed for each CpG site by testing the linkage between
 methylation status of adjacent reads, and the program <code>amrfinder</code> is
 used to identify regions with allele-specific methylation.
</p>
<p>
For more detailed description of the methods of each step, please
refer to the reference by Song et al. For instructions on how to use
<code>MethPipe</code>, you may obtain the <a href="http://smithlabresearch.org/manuals/methbase-manual.pdf">MethPipe Manual</a>.
</p>
</div>
"""

def get_credits_references(PMIDs):
    text = """
<h2>Credits</h2> 
<p>
The raw data were produced by {authors}. The data analysis were
performed by members of the Smith lab. 
</p>

<p>
Contact:
<a href="mailto:decato@usc.edu">Benjamin Decato</a> 
and 
<a href="mailto:lizji1992@gmail.com">Liz Ji</a>
</p>

<h2>Terms of Use</h2> 
<p>
If you use this resource, please cite us! The Smith Lab at USC has developed and is owner of all analyses and associated browser tracks from the MethBase database (e.g. tracks displayed in the "DNA Methylation" trackhub on the UCSC Genome Browser). Any derivative work or use of the MethBase resource that appears in published literature must cite the most recent publication associated with Methbase (see "References" below). Users who wish to copy the contents of MethBase in bulk into a publicly available resource must additionally have explicit permission from the Smith Lab to do so. We hope the MethBase resource can help you!
</p>

<h2>References</h2> 

<p><b>MethPipe and MethBase</b></p> <p>Song Q, Decato B, Hong E, Zhou M, Fang F, Qu J, Garvin T, Kessler M, Zhou J, Smith AD (2013) <a href="http://www.plosone.org/article/info:doi/10.1371/journal.pone.0081148">A reference methylome database and analysis pipeline to facilitate integrative and comparative epigenomics</a>. PLOS ONE 8(12): e81148</p>
<p><b>Data sources</b></p>
    """
    authors = []
    for PMID in PMIDs:
        citation = ""
        article = PubMedArticle(str(PMID))
        if article.valid:
            citation = "<p>{paper}</p>".format(paper = article.getMLAHtml())
            authors.append(article.authors[0] + " et al")
        text += citation
    text = text.format(authors = ', '.join(authors))
    return """
<div style="width:800px;">
{text}
</div>    
    """.format(text = text)
    
def get_html_footer():
    return """
</body>
</html>
"""

def toTableRow(s, assembly = ""):
    def get(v): return str(v) if v != None else ""
    def get_num(x): 
        try:
            return float(x)
        except:
            return 0

    if assembly == "tair10":
        text = """<tr>
<td>{ID}</td>
<td>{sample}</td>
<td>{strategy}</td>
<td>{bsrate:.3f}</td>
<td>{meanMethC:.3f}</td>
<td>{meanCoverage:.3f}</td>
<td>{coveredCpGs:.3f}</td>
<td>{coveredCs:.3f}</td>
<td>{numHyperMRCpG:.0f}</td>
<td>{numHyperMRAll:.0f}</td>
</tr>
"""
        return text.format(ID = get(s.ID), \
              sample = get(s.Description), \
              strategy = get(s.Strategy) if hasattr(s, 'Strategy') else '', \
              bsrate = get_num(s.bsRate), \
              meanMethC = get_num(s.meanMethC), \
              meanCoverage = get_num(s.meanCoverage), \
              coveredCpGs = get_num(s.coveredCpGs) / get_num(s.numCpGs), \
              coveredCs = get_num(s.coveredCs) / get_num(s.numCs), \
              numHyperMRCpG = get_num(s.numHyperMRCpG), \
              numHyperMRAll = get_num(s.numHyperMRAll))
    else:
        text = """<tr>
<td>{ID}</td>
<td>{sample}</td>
<td>{strategy}</td>
<td>{bsrate:.3f}</td>
<td>{meanMethCpG:.3f}</td>
<td>{meanCoverage:.3f}</td>
<td>{coveredCpGs:.3f}</td>
<td>{numHMR:.0f}</td>
<td>{numAMR:.0f}</td>
<td>{numPMD:.0f}</td>
<td>{Note}</td>
</tr>
"""
        notes = ""
        if not s.is_WGBS():
            notes += """<span style="font-weight:bold">non-WGBS;&nbsp</span>"""
        if get_num(s.bsRate) > 0.0 and get_num(s.bsRate) < 0.95:
            notes += """<span style="color:red">LowBS;&nbsp</span>"""
        if get_num(s.meanCoverage) > 0.0 and get_num(s.coveredCpGs)  > 0 \
           and get_num(s.numCpGs) > 0.0: 
            meanCoverage = get_num(s.meanCoverage)
            if not s.is_WGBS(): meanCoverage = meanCoverage*get_num(s.numCpGs)/get_num(s.coveredCpGs)
            if meanCoverage < 6.0:
                notes += """<span style="color:darkorange">LowCov;&nbsp</span>"""
        notes += """<a href="{url}">Download</a>""".format(url=s.URL.replace("results_", "tracks_"))
            
        return text.format(ID = get(s.ID), \
                sample = get(s.Description), \
                strategy = get(s.Strategy) if hasattr(s, 'Strategy') else '', \
                bsrate = get_num(s.bsRate), \
                meanMethCpG = get_num(s.meanMethCpG), \
                meanCoverage = get_num(s.meanCoverage), \
                coveredCpGs = get_num(s.coveredCpGs) / get_num(s.numCpGs) if get_num(s.numCpGs) > 0 else 0.0, \
                numHMR = get_num(s.numHMR), \
                numAMR = get_num(s.numAMR), \
                numPMD = get_num(s.numPMD), \
                Note = notes)

def main():

#==============================================================================================
    parser = argparse.ArgumentParser(description='genreate a summary for specific celltype.')
    parser.add_argument('-a', '--assembly', dest = 'assembly', required = True,
                        help = 'assembly id, e.g., hg18')
    parser.add_argument('-c', '--celltype', dest = 'celltype', required = True,
                        help='which cell type track you want to set up')
    parser.add_argument('-f', '--file', dest = 'file_organization',
                        default = '/labdata/methylome/public/Cell_Type/organization.txt', 
                        help='organization file to construct cell type projects')
    parser.add_argument('-d', '--dir', dest = 'dir', 
                        default = '/labdata/methylome/public/',
                        help='top level directory containing projects')
    parser.add_argument('-o', '--output', dest = 'html_file', default = '',
                        help='html output file')

#==============================================================================================
    args = parser.parse_args()
   
    file_organization = args.file_organization
    assembly = args.assembly
    celltype = args.celltype

    (sample_dirs, projects) = getSampleDirs(file_organization, celltype, args.dir, assembly)

    if len(sample_dirs) == 0:
        sys.stderr.write("No results for assembly:" + assembly + "\n")
        sys.exit(-1)
    
    html_file = args.html_file
    f = open(html_file, "w")
    f.write(get_html_header(celltype))
    if assembly == "tair10":
        f.write("""
<h2> Description </h2>    
<table>
<tr>
<th>Sample</th>
<th></th>
<th></th>
<th>BS rate</th>
<th>Methylation</th>
<th>Coverage</th>
<th>%CpGs</th>
<th>%Cs</th>
<th>#HyperMR-CpG</th>
<th>#HyperMR-All</th>
</tr>
""")
    else:
        f.write("""
<h2> Description </h2>    
<table>
<tr>
<th>Sample</th>
<th></th>
<th></th>
<th>BS rate*</th>
<th>Methylation</th>
<th>Coverage</th>
<th>%CpGs</th>
<th>#HMR</th>
<th>#AMR</th>
<th>#PMD</th>
<th></th>
</tr>
""")

    RRBS_flag = False
    Low_conversion_flag = False
    Low_coverage_flag = False
    PMIDs = []
    for sample_dir in sample_dirs:
        s = Sample()
        s.getMetaDataFromDir(sample_dir)
        tablerow = toTableRow(s, assembly) 
        f.write(tablerow)
        if tablerow.find(">RRBS;") != -1: RRBS_flag = True
        if tablerow.find(">LowBS;") != -1: Low_conversion_flag = True
        if tablerow.find(">LowCov;") != -1: Low_coverage_flag = True
        if s.PMID != None and s.PMID not in PMIDs:
            PMIDs.append(s.PMID)
    
    for p in projects:
        if p.PMID != None and p.PMID not in PMIDs:
            PMIDs.append(p.PMID)

    f.write("</table>\n") 
    f.write("""<br>* see Methods section for how the bisulfite conversion rate is calculated""")
    if RRBS_flag or Low_coverage_flag or Low_conversion_flag:
        f.write("<br>Sample flag:")
    if RRBS_flag:
        f.write("""<br><span style="font-weight:bold">RRBS:&nbsp</span> sample is generated with reduced representation bisulfite sequencing (RRBS);""")
    if Low_conversion_flag:
        f.write("""<br><span style="color:red">LowBS:&nbsp</span> sample has low bisulfite conversion rate (<0.95);""") 
    if Low_coverage_flag:
        f.write("""<br> <span style="color:darkorange">LowCov:&nbsp</span> sample has low mean coverage (<6.0)""") 
    f.write(get_conventions())
    f.write(get_methods())
    f.write(get_credits_references(PMIDs))
    f.write(get_html_footer())
    f.close()
 
if __name__ == '__main__':
    main()
