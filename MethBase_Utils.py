# MethBase_Utils
#
# Copyright (C) 2013-2017 University of Southern California and
#                         Song Qiang <keeyang@ustc.edu>
#                         Liz Ji <lizji1992@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import os

class MyConfigParser:
    def read(self, fn):
        self.items = []
        for line in open(fn):
            fields = []
            comment_start = line.find("#")
            if comment_start != -1:
                line = line[:comment_start]
            if line.find("=") != -1:
                fields = line.split("=", 1)
            elif line.find(":") != -1:
                fields = line.split(":", 1)
            elif line.find("\t") != -1:
                fields = line.split("\t", 1)
            elif line.find(" ") != -1:
                fields = line.split(" ", 1)

            if len(fields) == 2 and fields[0].strip() and fields[1].strip():
                self.items.append((fields[0].strip(), fields[1].strip()))

    def __init__(self, fn):
        self.read(fn)

def isYAMLConfig(file):
    flag = False
    IN = open(file)
    header = IN.readline()
    if header.find('YAML') != -1:
        flag = True
    IN.close()
    return flag

def tab2spaces(file, offset=4):
    IN = open(file)
    tmpfile = file+'.tmp'
    TMPOUT = open(tmpfile, 'w')
    text = IN.read().replace('\t', ' '*offset)
    TMPOUT.write(text)
    os.remove(file)
    os.rename(tmpfile, file)
