\documentclass[11pt]{article}

\usepackage{indentfirst, times, fullpage, graphicx, subfigure, natbib,
  setspace, amsmath, multirow, algorithm, epstopdf, amsmath, amsthm,
  amssymb}
%% \usepackage{endfloat}

\usepackage[pdfborder={0 0 0}]{hyperref}

%% style="width:800px; margin:auto"
%% width="688"

%% For program names
\newcommand{\prog}[1]{\texttt{#1}}

\title{MethBase: a reference methylome database}
\author{}
\date{}

\begin{document}
\maketitle

MethBase is a central reference methylome database created from public
BS-seq datasets. It contains hundreds of methylomes from well studied
organisms. For each methylome, Methbase provides methylation level at
individual sites, hypo- or hyper-methylated regions, partially
methylated regions, allele-specifically methylated regions, and
detailed meta data and summary statistics. These results are generated
with the \href{http://smithlab.usc.edu/methbase/}{MethPipe} software package,
a standalone, comprehensive pipeline for analyzing BS-seq data, both
WGBS and RRBS.

\section{Access MethBase}
\label{sec:accessing-methbase}
MethBase is publicly available to the scientific community as a track
hub in the UCSC Genome Browser. If you are using the main site of UCSC
Genome Browser, the MethBase track hub is built in by default, you may
select the MethBase tracks from the Public Hubs section of the UCSC
Genome Browser (Figure~\ref{fig:public-hub}). If you are using the a
mirror site of UCSC Genome Browser, you may make the MethBase tracks
available by loading the configuration file
\url{http://smithlab.usc.edu/trackdata/methylation/hub.txt}
(Figure~\ref{fig:add-hub}). Please refer to
\href{http://genome.ucsc.edu/goldenPath/help/hgTrackHubHelp.html}{Using
  UCSC Genome Browser Track Hubs} for how to view unlisted hubs.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=6in]{/home/qiangson/Documents/methylome/docs/methpipe/figs/snapshot-select-methbase-public-hub.pdf}
  \caption{Select MethBase hub track from UCSC Genome Browser main site}
  \label{fig:public-hub}
\end{figure}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=6in]{/home/qiangson/Documents/methylome/docs/methpipe/figs/snapshot-add-methbase-hub.pdf}
  \caption{Add MethBase track hub in mirror sites of UCSC Genome
    Browser}
  \label{fig:add-hub}
\end{figure}

\clearpage
\section{Select tracks for viewing}
\label{sec:select-tracks-view}
After you load the MethBase track hub setting file and go to the
Genome Browser page, it shows the methylation level tracks and the HMR
tracks of typical methylomes (Figure~\ref{fig:features}A). 

\begin{figure}[t!]
  \centering
  \includegraphics{/home/qiangson/Documents/methylome/docs/methpipe/figs/Chart.pdf}
  \caption{Examples of high-level methylation features available in
    MethBase through the UCSC Genome Browser track hub: (A)
    hypo-methylated regions (HMRs); (B) hyper-methylated regions
    (HyperMRs); (C) partially methylated domains (PMDs), and (D)
    allele-specific methylated regions (AMRs).}
  \label{fig:features}
\end{figure}

% \begin{figure}[htbp]
%   \centering
%   \includegraphics[width=6in]{/home/qiangson/Documents/methylome/docs/methpipe/figs/snapshot-default-tracks.pdf}
%   \caption{Default methylation tracks and HMRs tracks for representive
%   sample from each project}
%   \label{fig:default-track}
% \end{figure}

%% turn on/off additional samples and tracks from a project
Besides the default methylation tracks and HMR tracks, MethBase also
provides a variety of other tracks, including coverage,
hyper-methylated regions (Figure~\ref{fig:features}B), partially
methylated domains (Figure~\ref{fig:features}C) and allele-specific
methylated AMRs (Figure~\ref{fig:features}D). To display those tracks,
you may go to the track setting page in one of the three ways: 1)
click the project name form the list of projects in
Figure~\ref{fig:list-project}; 2) right click on of the viewable
tracks, and select ``Configure track settings'' from the manu; or 3)
go to the track description page and click the link ``Go to track
controls''. In the track setting page
(Figure~\ref{fig:track-setting}), you can select additional samples
from that project and modify the display settings of more types of
data.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=6in]{/home/qiangson/Documents/methylome/docs/methpipe/figs/snapshot-track-settings.pdf}
  \caption{Track setting interface for selectting additional samples
    and tracks}
  \label{fig:track-setting}
\end{figure}

%% turn off/off methylomes from a study 
Below the Browser, you will a list of public methylomes in MethBase
under the \textbf{DNA Methylation} section
(Figure~\ref{fig:list-project}). From the list, you may modify the
display settings of several samples from a project.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=6in]{/home/qiangson/Documents/methylome/docs/methpipe/figs/snapshot-set-project-tracks.pdf}
  \caption{List of projects available in the MethBase, from the
    display settings of all tracks from that sample can be modified}
  \label{fig:list-project}
\end{figure}

\clearpage
\section{View meta data and summary statistics}
\label{sec:viewing-meta-data}
You can click on any track to go to the Description page for that
track, which gives detailed meta and summary statistics about that
methylome (Figure~\ref{fig:meta-data}).

\begin{figure}[htbp]
  \centering
  \includegraphics[width=6in]{/home/qiangson/Documents/methylome/docs/methpipe/figs/MethBaseMeta2.pdf}
  \caption{Detailed meta data and summary statistics of methylomes}
  \label{fig:meta-data}
\end{figure}

\section{Download data for further analysis}
\label{sec:downl-data-furth}
There are two ways to download data files from MethBase. First, you
may go to the Table Browser to download the data files of any tracks
for additional analysis (Figure~\ref{fig:download-data}).

\begin{figure}[htbp]
  \centering
  \includegraphics[width=6in]{/home/qiangson/Documents/methylome/docs/methpipe/figs/snapshot-dowbnload-table.png}
  \caption{Download data from the Table Browser}
  \label{fig:download-data}
\end{figure}

Alternatively, you may click the \textbf{Download} link from the
Description page of each methylome (Figure~\ref{fig:meta-data}), which
will direct you to the track data directory. The *.meth.bw and
*.read.bw files contain methylation level and coverage information
respectively at each CpG site. The *.hmr.bb, *.pmd.bb, *.amr.bb files
contain HMRs, PMDs and AMRs, and the *.allelic.bw file gives the
allelic score at each CpG site. To extract text files from these
bigWig and bigBed files, you may use the
\href{http://genome.ucsc.edu/goldenPath/help/bigWig.html}{bigWigToBedGraph}
and
\href{http://genome.ucsc.edu/goldenPath/help/bigBed.html}{bigBedToBed}
utilities available from
\href{http://hgdownload.cse.ucsc.edu/admin/exe/}{UCSC server}.

\section{Contact}
\label{sec:contact}
Please send emails to the methpipe mailing list at
\textbf{methpipe@googlegroups.com} if you have any questions,
suggestions or comments. 

\section{References}
\label{sec:references}
Song Q, Decato B, Hong E, Zhou M, Fang F, Qu J, Garvin T, Kessler M,
Zhou J, Smith AD (2013) A reference methylome database and analysis
pipeline to facilitate integrative and comparative epigenomics. PLOS
ONE (in press)
\href{http://smithlab.usc.edu/plone/publications/pdfs/Songetal2013PONEMethPipe.pdf}{[PDF]}

\end{document}

