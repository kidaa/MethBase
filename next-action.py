# next-action.py
#
# Copyright (C) 2013-2017 University of Southern California and
#                         Song Qiang <keeyang@ustc.edu>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python

# Import modules for CGI handling 
import cgi
import cgitb 
cgitb.enable()

class Sample:
    def __init__(self, str):
        fields = str.split(",")
        self.ID = fields[0]
        self.organism = fields[1]
        self.cellType = fields[2]
        self.state = fields[3]
        self.producer = fields[4]
        self.PMID = fields[5]
        self.SEPE = fields[6]
        self.kit = fields[7]
        self.readLength = fields[8]
        self.bsRate = fields[9]
        self.meanCoverage = fields[10]
        self.coveredCpGs = fields[11]
        self.coveredCs = fields[12]
        self.meanMethC = fields[13]
        self.meanMethCpG = fields[14]
        self.numHMR = fields[15]
        self.numAMR = fields[16]

    def toHtmlTableRow(self):
        line = "<tr>"
        line = line + "<td>{0}</td>".format(self.ID) \
            + "<td>{0}</td>".format(self.organism) \
            + "<td>{0}</td>".format(self.cellType) \
            + "<td>{0}</td>".format(self.state) \
            + "<td>{0}</td>".format(self.producer) \
            + "<td>{0}</td>".format(self.PMID)
        line = line + "</tr>"
        return line
    
def print_header():
    print "Content-type: text/html\n\n"
    print "<html>"
    print "<head>"
    print "<title>List of Methylomes: </title>"
    print "</head>"
    print "<body>"
    print "<h1>MethBase: gateway to hundreds of reference methylomes</h1>"


def print_footer():
    print "</body>"
    print "</html>"

def visualize_in_browser():
    print "generating browser configuration files"

def generate_download_data():
    print "generating file list for download"
    
def do_exceptions():
    print "no action selected. aborted"
    
if __name__ == '__main__':

    # Create instance of FieldStorage 
    form = cgi.FieldStorage() 
    
    print_header()

    if "nextAction" in form:
        if form["nextAction"] == "Show in Genome Browser":
            visualize_in_browser()
        else:
            generate_download_data()
    else:
        do_exceptions()


##    if "tracks" in form:
##        tracks = form.getlist("tracks")
##        for t in tracks:
##            print t,"<br>"
##
##    if "samples" in form:
##        samples = form.getlist("samples")
##        for t in samples:
##            print t,"<br>"
##            
    print_footer()
