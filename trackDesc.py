#!/usr/bin/python

# trackDesc.py: generate an html file with summary of a BS-seq sample
#
# Copyright (C) 2013-2017 University of Southern California and
#                         Song Qiang <keeyang@ustc.edu>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import string
import os

from MethBase import Sample
import projectDesc

def get_sample_desc(s):
    desc = """
<h2>  {ID} </h2>
<h3>Description</h3>
<table>
<tr><td>Organism</td><td>{organism}</td></tr>
<tr><td>Sample</td><td>{sample}</td></tr>
<tr><td>Authors</td><td>{authors}</td></tr>
<tr><td>PubMed</td><td>{pubmed}</td></tr>
<tr><td>SRA</td><td>{SRA}</td></tr>
<tr><td>Technique</td><td>{technique}</td></tr>  
</table>

<h3>Methylome summary</h3>
<table>
<tr><td>read length</td><td>readLength</td></tr>
<tr><td>Conversion rate</td><td>{bsrate}</td></tr>
<tr><td>Methylation</td><td>{meanMethCpG}</td></tr>
<tr><td>Coverage</td><td>{meanCoverage}</td></tr>
<tr><td>Coverage CpGs</td><td>{coveredCpGs}</td></tr>
<tr><td>#HMR</td><td>{numHMR}</td></tr>
<tr><td>#AMR</td><td>{numAMR}</td></tr>
<tr><td>#PMD</td><td>{numPMD}</td></tr>
</table>

"""
    def get(v): return str(v) if v != None else "NA"
    return desc.format(ID = get(s.ID), \
                       organism = get(s.organism), \
                       sample = get(s.sample), \
                       authors = s.authors[0] if s.authors != None else "NA", \
                       pubmed = get(s.PMID), \
                       SRA = get(s.SRAID), \
                       technique = get(s.kit) + "; " + get(s.SEPE), \
                       readLength = get(s.readLength), \
                       bsrate = get(s.bsRate), \
                       meanMethCpG = get(s.meanMethCpG), \
                       meanCoverage = get(s.meanCoverage), \
                       coveredCpGs = get(s.coveredCpGs), \
                       numHMR = get(s.numHMR), \
                       numAMR = get(s.numAMR), \
                       numPMD = get(s.numPMD))

def main():
    sample_dir = os.path.abspath(sys.argv[1])
    s = Sample()
    s.getMetaDataFromDir(sample_dir)
    html_file = os.path.join(s.Dir, s.ID + ".html")
    f = open(html_file, "w")
    f.write(projectDesc.get_html_header(s.ID))
    f.write(get_sample_desc(s))
    f.write(projectDesc.get_methods())
    f.write(projectDesc.get_html_footer())
    f.close()
    
if __name__ == '__main__':
    main()
