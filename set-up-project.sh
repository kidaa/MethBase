# set-up-project.sh: set up a project directory to be included in MethBase 
#
# Copyright (C) 2013-2017 University of Southern California and
#                         Song Qiang <keeyang@ustc.edu>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#! /bin/bash

function setup_dir # project_dir assembly  
{
    projdir=$(readlink -f $1);
    id=$(basename $projdir|sed 's/-/_/g');
    assembly=$2;
    
    # updating descrition file
    /usr/local/bin/python ~qiangson/Documents/methbase/projectDesc.py  \
	-d $projdir  -a $assembly -o $projdir/$id-$assembly.html;
    
    linkfile=/labdata/methylome/public/methbase/trackhub_test/$assembly/$id.html;      
    [ ! -L "$linkfile" ] && ln -f -s $projdir/$id-$assembly.html $linkfile;   
}

cd /labdata/methylome/public;

if [ $# == 0 ];
then
    echo "Usage: set-up-project proj_dir assembly";
    echo "Example: set-up-project Hodges-Human-2011 hg19";
    exit 1;		
else
    echo "Setting up" $1;
    setup_dir $1 $2;
fi



